# 第一堂分享： 接口测试之 http 起步

> 接口测试：模拟端（web\app）程序向服务端请求，验证数据
## 工作中使用的接口测试方式

1.  手动发送 http 请求

    通过 postman 、httpclient 、curl 发送数据验证响应

2.  编写完整的流水线业务

    使用提供的框架(Iframework)，组织用例，断言响应，加入流水线

## 接口流水线测试忠实套路

![](img/exceltest.jpg)

## 接口测试用例编写套路

1.  找准接口地址及序列化方式

    包括两个地址，一个是根路径（域名），一个是接口路径（/getHotWord）
    application/json,application/x-www-form-urlencoded

2.  完善请求参数

    也就是详细的入参，如：https://m.zbj.com/shunt/favorite?size=50
    参数只有size

3.  得到响应结果

    也就是接口返回参数，如: {"success":true, "content": "", errCode=0, errMsg="返回成功"}

4.  验证规则

    也就是业务逻辑，如上面的接口，size=50，返回的pageSize也要等于50

## 小试牛刀

1.  get 方式
    - postman
    ![](img/postman.png)
    - iframework
2.  post 方式
    - application/json
    ![](img/json.png)
    - application/x-www-form-urlencoded
    ![](img/url.png)
    - iframework
## 课堂链接
. [http飞升精华篇](session2.md)