## 一、 maven 项目搭建及框架骨架配置
* 1、打开Idea新建项目，选择Maven

<img src="img/01.jpg" width="60%"/>

* 2、勾选 Create From Archetype，点击Add Archetype

<img src="img/02.jpg" width="60%"/>

<img src="img/03.jpg" width="60%"/>

* 3、配置：

<img src="img/04.jpg" width="30%"/>

      GroupId：com.zhubajie.autotest.plugins

      ArtifactId：autotest-plugin-archetype

      Version：1.0.0-SNAPSHOT

配置完成点击下一步，项目命名，创建完成,项目创建完成根据框架依赖自动拉取框架架包到本地。
## 二、测试用例生成以及测试项目的配置
- 1、找开发大大获取到接口文档所在的wiki地址

wiki地址：http://doc.zhubajie.la/pages/viewpage.action?pageId=40328766

<img src="img/05.png" width="70%"/>

- 2、根据接口文档信息获取到对应测试项目接口API的maven依赖，在把maven依赖添加到创建好的测试项目的pom.xml配置文件中

<img src="img/06.png" width="70%"/>

- 3、根据接口文档找到要测试的接口，根据接口信息测试对应的测试用例

<img src="img/07.png" width="70%"/>

- 4、生成测试用例后编辑test.properties文件

<img src="img/08.png" width="70%"/>

> profile：表示测试环境： test对应测试环境、testing对应预发布环境、maintest对应主测试环境、

> projectName：表示开发的项目名称，如果不知道测试的项目名称是什么可以问开发大大或者在devops流水线平台查询

> runner：表示测试人员名称

> serviceVersion：版本（若该接口包含版本号则写入接口版本）
## 三、测试用例的编写
1、获取测试用例编写前所需要的信息
如：
* 根据接口文档获取详细的入参出参信息、包括入参出参每个字段所表示的意义
* 了解接口会操作哪些数据库，哪些表（信息来源可以问开发大大）
* 了解接口会不会涉及到第三方接口的调用（信息来源可以问开发大大）
* 理解接口的业务逻辑和对应的功能作用

列：金石会员抽奖接口

<img src="img/09.png" width="70%"/>

2、测试用例编写前的准备自己的测试数据（便于接口自动化持续集成）代码如下：

方式1:

```java
public void beforeTest(Map<String, Object> context) {
		 NewSqlTools.newInstance()
         // 连接数据库
         .connect("数据库名")
         // 执行数据库 R(增、删、改、查) 操作
         .execute("REPLACE INTO `表名`（`字段1`，`字段2`，...）values (`属性值1`，`属性值2`，...)")
         // 关闭数据库连接
         .disconnect();
	}
```
方式2：

```Java
public void beforeTest(Map<String, Object> context) {
	
      String sql="REPLACE INTO `表名` (`字段1`，`字段2`，...) VALUES (`属性值1`，`属性值2`，...)";
      // 连接数据库
      newSqlTools.connect("zhubajie_member").execute(sql).disconnect();
	}
```
### 3、根据接口文档及业务逻辑对接口入、出参，做校验断言
* 3.1、利用框架本身对接口如参进行简单参数校验

<img src="img/10.png" width="70%"/>

例：按预期结果正常抽奖

<img src="img/11.png" width="70%"/>

必传入参校验失败：

<img src="img/12.png" width="70%"/>

* 3.2、会员抽奖接口用例：

<img src="img/13.png" width="70%"/>

出参断言代码如下：

```Java
    @Test(dataProvider = Data.SINGLE, enabled = true)
    @RollBack(dbName = "zhubajie_ad_base", tableName = {"ad_lottery_activity", "ad_lottery_record", "ad_user_point_dtl"})
    public void testCase(Map<String, Object> context, Result<ResUserPrizeDto> result) {
        Result<ResUserPrizeDto> data = (Result<ResUserPrizeDto>) result;
        ResUserPrizeDto draw = result.getData();
        sqlTools.connect( "zhubajie_ad_base", "172.20.20.104", "root", "zhubajie" );
        if (context.get( "caseName" ).toString().equals( "成功抽奖" )) {
            boolean success = true;
            boolean getSuccess = draw.getSuccess();
            Assert.assertEquals( getSuccess, success, "抽奖失败" );
            List<Map<String, Object>> map = sqlTools.queryForMapList( "select * from `ad_lottery_record` where `fws_id` =19182358 order by id desc" );
            Map<String, Object> getMap = map.get( 0 );
            Integer prizelevel = Integer.parseInt( getMap.get( "prize_level" ).toString() );
            Integer PrizeRank = draw.getPrizeRank();
            Assert.assertEquals( prizelevel, PrizeRank, "奖品等级类型不一致" );
        } else if (context.get( "caseName" ).toString().equals( "活动未开始" )) {
            boolean success2 = false;
            boolean getSuccess = draw.getSuccess();
            Assert.assertEquals( getSuccess, success2, "未开始活动抽奖状态不一致" );
            List<Map<String, Object>> map = sqlTools.queryForMapList( "select * from `ad_lottery_activity`" );
            Map<String, Object> getMap = map.get( 15 );
            String activitybegintime = getMap.get( "activity_begin_time" ).toString();
            SimpleDateFormat sf = new SimpleDateFormat( "yyyy-MM-dd" );
            try {
                Date begintime = sf.parse( activitybegintime );
                if (System.currentTimeMillis() < begintime.getTime()) {
                    String Msg = "活动尚未开始";
                    String getErrMsg = draw.getErrMsg();
                    Assert.assertEquals( getErrMsg, Msg, "活动未开始时间判断不一致" );
                }
            } catch (Exception e) {
            }
        } else if (context.get( "caseName" ).toString().equals( "积分不足" )) {
            Integer X = 10;
            boolean success3 = false;
            boolean getSuccess = draw.getSuccess();
            Assert.assertEquals( getSuccess, success3, "积分不足抽奖状态返回成功" );
            List<Map<String, Object>> map = sqlTools.queryForMapList( "select sum(real_point) from `ad_user_point_dtl` WHERE `user_id` =19182351" );
            Map<String, Object> getMap = map.get( 0 );
            Integer integral = Integer.parseInt( getMap.get( "sum(real_point)" ).toString() );
            if (integral < X) {
                String Msg2 = "积分不足";
                String getErrMsg = draw.getErrMsg();
                Assert.assertEquals( getErrMsg, Msg2, "用户积分不足判断判断不一致" );
            }
        }
        sqlTools.disconnect();
    }
}
```


