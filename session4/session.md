# framework-autotest调用链
![](img/master_auto_test.png)
## 第一层：每个接口用例继承基类接口
1. Dubbo接口继承BaseDubbo
2. HTTP接口继承BaseRestful
### BaseDubbo基类
> 作用：在每个case执行之前做一些前置性的处理，正因为如此使用了TestNG IHookable类
1. 初始化测试前置条件
    - 所有用例开始前打开数据库
    - 所有用例结束后关闭数据
    - 初始化Dubbo服务
3. 获取提供的测试类信息（反射）
4. 初始化接口入参、场景、校验信息
5. 执行用例、进行断言（缓存接口信息）
7. 生成上传报告
### BaseRestful基类
基本流程和上面的一样
> rest主要使用restassured，[详细使用参考链接](https://testerhome.com/topics/7060)
## 第二层各种TestNG监听
### 测试用例执行过程中的监听器，都继承[TestListenerAdapter](https://jitpack.io/com/github/cbeust/testng/master/javadoc/org/testng/TestListenerAdapter.html)
1. ScenarioRollBackListener（通用测试场景数据回滚监听器）
>场景开始结束前后执行
2. RollBackListener （数据回滚监听器）
>case开始结束前后执行
3. SaveResultListener （测试数据入库监听器）
>case执行完将各种测试结果输出
### 报告监听器
ReportListener （通用测试结果处理监听器）
>根据extendreport组织测试结果