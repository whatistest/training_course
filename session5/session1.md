
<center>UI自动化入门</center>
===
# **1.selenium发展史**
---
        Jason Huggins在2004年发起了Selenium项目，当时身处ThoughtWorks的他，为了不想让自己的时间浪费在无聊的重复性工作中，幸运的是，所有被测试的浏览器都支持Javascript。Jason和他所在的团队采用Javascript编写一种测试工具来验证浏览器页面的行为；这个JavaScript类库就是Selenium core，同时也是seleniumRC、Selenium IDE的核心组件。Selenium由此诞生。

<center><img src="img/selenium.png" width="70%"/></center>


## **Selenium 1.0**
<center><img src="img/selenium1.0.png" width="70%"/></center>

用简单的公式：

Selenium 1.0 = Selenium IDE + Selenium Grid + Selenium RC

## **Selenium IDE 1.0**

Selenium IDE是嵌入到Firefox浏览器中的一个插件，实现简单的浏览器操作的录制与回放功能。

## **Selenium Grid 1.0**

        Selenium Grid是一种自动化的测试辅助工具，Grid通过利用现有的计算机基础设施，能加快Web-App的功能测试。利用Grid可以很方便地实现在多台机器上和异构环境中运行测试用例。


## **Selenium RC**

        Selenium RC（Remote Control）是Selenium家族的核心部分。Selenium RC 支持多种不同语言编写的自动化测试脚本，通过Selenium RC的服务器作为代理服务器去访问应用，从而达到测试的目的。

<center><img src="img/seleniumRc.png" width="70%"/></center>

        在2006年的时候，Google的工程师Simon Stewart发起了WebDriver的项目；因为长期以来Google一直是Selenium的重度用户，但却被限制在有限的操作范围内。

Selenium RC 是在浏览器中运行JavaScript应用，使用浏览器内置的JavaScript翻译器来翻译和执行selenese命令（selenese是Selenium命令集合）。

        WebDriver是通过原生浏览器支持或者浏览器扩展来直接控制浏览器。WebDriver针对各个浏览器而开发，取代了嵌入到被测Web应用中的JavaScript，与浏览器紧密集成，因此支持创建更高级的测试，避免了JavaScript安全模型导致的限制。除了来自浏览器厂商的支持之外，WebDriver还利用操作系统级的调用，模拟用户输入。

        Selenium与WebDriver原是属于两个不同的项目，WebDriver的创建者Simon Stewart早在2009年8月的一份邮件中解释了项目合并的原因。

## **Selenium 2.0**

        因为Selenium和Webdriver的合并，所以，Selenium 2.0由此诞生。简单用公式表示为：Selenium 2.0 = Selenium 1.0 + WebDriver

        需要强调的是，在Selenium 2.0中主推的是WebDriver，可以将其看作Selenium RC的替代品。因为Selenium为了保持向下的兼容性，所以在Selenium 2.0中并没有彻底地抛弃Selenium RC。所以，我们在学习Selenium2.0的时候，核心是学习WebDriver。它的工作原理是这样的：

<center><img src="img/selenium2.0.png" width="70%"/></center>

## **Selenium 3.0**

2016年7月，Selenium3.0悄悄发布第一个beta版。

* 终于去掉了RC，简单用公式表示为：Selenium 3.0 = Selenium 2.0 + Selenium RC（Remote Control）
* Selenium3.0只支持Java8版本以上。
* Selenium3.0中的Firefox浏览器驱动独立了，以前装完selenium2就可以驱动Firefox浏览器了，现在和Chrome一样，必须下载和设置浏览器驱动。
* MAC OS 集成Safari的浏览器驱动。默认在/usr/bin/safaridriver 目录下。
* 只支持IE 9.0版本以上。

# **2.selenium环境搭建**
---
## **2.1 安装JDk**
下载地址：
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

安装和环境变量配置自行补脑，网上一大堆

 <center><img src="img/yes.gif" width="20%"/></center>

 ## **2.2 Eclipse下载**

 下载地址：https://www.eclipse.org/downloads/

 ## **2.3 selenium下载**

下载地址：http://mvnrepository.com/artifact/org.seleniumhq.selenium

<center><img src="img/repository.png" width="70%"/></center>

 ## **2.4 testNG下载**
下载地址：http://mvnrepository.com/artifact/org.testng/testng

<center><img src="img/testNG.png" width="70%"/></center>


 ## **2.5 maven下载**
下载地址：http://maven.apache.org/download.cgi

<center><img src="img/maven.png" width="70%"/></center>

下载完成后解压，然后配置环境变量，配置完后在Eclpse中设置一下maven。maven的setting.xml文件配置仅供参考：https://www.cnblogs.com/zengming/p/7786684.html

# **3.简单的Case**
---
## **3.1 自动化用例编写步骤**
a、声明driver对象————>启动浏览器

b、driver去打开浏览器并输入你要测试的网页地址————>打开网页

c、利用WebElement声明元素对象————>找到要输入的文本框、要点击的按钮等等

d、对元素进行输入、点击、断言操作————>对找到文本框、按钮等进行操作

e、关闭浏览器，释放资源————>关闭浏览器




```Java
 //设置驱动
	//设置驱动
	  //System.setProperty("webdriver.chrome.driver","./resource/chromedriver.exe");
	  System.setProperty("webdriver.firefox.bin","D:/Mozilla Firefox/firefox.exe");
	  // System.setProperty("webdriver.gecko.driver", "E:/geckodriver.exe");
	  //声明driver，并打开网站
	  // WebDriver driver=new ChromeDriver();
	  WebDriver driver=new FirefoxDriver();
	  driver.get("http://www.baidu.com");
	  //利用WebElement声明元素对象
	  WebElement element=driver.findElement(By.id("kw"));
	  //对元素进行输入、点击、断言操作
	  element.sendKeys("selenium自动化用例");
	  //使线程休眠5秒
	  try {
		Thread.sleep(5000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  //关闭浏览器
	  driver.close();
```
chromedriver下载地址：https://blog.csdn.net/huilan_same/article/details/51896672

geckodriver下载地址：https://github.com/mozilla/geckodriver/releases/

## **3.2 元素定位**

* 使用插件：xPath Finder

<center><img src="img/index.png" width="70%"/></center>

例子：


```Java
System.setProperty("webdriver.firefox.bin","D:/Mozilla Firefox/firefox.exe");
WebDriver driver=new FirefoxDriver();
driver.get("http://www.baidu.com");
WebElement element=driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[3]/a[1]"));
element.click();
```
* 手写xPath

        参照ppt

* 定位方式

        a、通过id定位
    
        <input type="text" autocomplete="off" suggestwidth="528px" id="input" class="placeholder" name="q">
        通过id的定位方式就是：By.id(“input”),然后用WebElement接收对象代码如下：

        WebElement element = driver.findElement(By.id("input"));这样这个元素就算获取到了。

        b、通过classname定位

        <input type="text" class="input-box" name="w">
        那么我们改成用classname定位方式就是：By.className(“input-box”), 然后用WebElement接收对象代码如下：

        WebElement element = driver.findElement(By.className("input-box"));

        c.通过name定位

        <input autocomplete="off" maxlength="255" value="" class="s_ipt" name="wd" id="kw">
        其中有name这个属性，我们可以利用这个属性定位输入框：By.name(“wd”);
        WebElement接收对象代码如下：

        WebElement element = driver.findElement(By.name("wd"));

        d.通过xpath定位

        <input autocomplete="off" maxlength="255" value="" class="s_ipt" name="wd" id="kw">
        那么我们用xpath定位元素可以写成：

        By.xpath(“//*[@name=’wd’]”);或者By.xpath(“//*[@id=’kw’]”);

        e.通过cssSelector定位

        自行补脑。。。。。。
 <center><img src="img/yes.gif" width="20%"/></center>






