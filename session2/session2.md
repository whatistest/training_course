# 开发相关基础知识
## JAVA
### 处理环境
> JAVA开发相关环境主要是JDK, Maven, Gradle

JDK(JAVA Development Kit)中文：开发工具包
- 下载
- 配置环境变量(为什么需要配置环境变量？)
从名称解释上我们已经了解到JDK是开发工具包，就需要告诉[操作系统]，你执行编写的程序，到那个路径下读取这些依赖文件。
- 验证环境
- 开发工具的选择，我们是运用来更好的工作(Money)，建议用[IDEA](https://www.jetbrains.com/idea/?fromMenu)更智能，http://idea.lanyus.com/

[参考链接](../session1/session2.md)
### 厉害的Hello World!
```
package com.zhubajie.autotest;

public class TestMain {

    public static void main(String[] args) {
        System.out.printf("Hello World!");
    }
}
```
### JAVA SE(Java标准)[基础教程参考链接](http://www.runoob.com/java/java-tutorial.html)
JAVA是一门面向对象的高级编程语言，从测试工作中构建场景的角度出发，你需要掌握基本[数据类型](http://www.runoob.com/java/java-basic-datatypes.html)、[变量类型](http://www.runoob.com/java/java-basic-datatypes.html)、[判断遍历操作](http://www.runoob.com/java/java-loop.html)、[数组、集合](http://www.runoob.com/java/java-array.html)、[对象与类](http://www.runoob.com/java/java-object-classes.html)、[assert断言](http://junit.sourceforge.net/javadoc_40/org/junit/Assert.html)

### JAVA常用指令介绍
1. jconsole
2. java -jar xxx.jar
3. jheap
4. jmap
5. jps
6. jstat
## Python
### 处理环境
> python[起源](https://zh.wikipedia.org/wiki/Python)说明，简单总结就是为懒人设计的语言

[安装参考链接](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/0014316090478912dab2a3a9e8f4ed49d28854b292f85bb000)
### 厉害的Hello World!
```
#!/usr/bin/env python
# -*- coding: utf-8 -*-

print('Hello World!')
```
### Python([基础教程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000))
平时工作中使用python机会不多，可以根据提供的教程一步一步来就可以了
## Nodejs
### 处理环境
[安装参考链接](http://www.runoob.com/nodejs/nodejs-install-setup.html)
### 第一个Hello World!
```
var http = require('http');

http.createServer(function (request, response) {

    // 发送 HTTP 头部 
    // HTTP 状态值: 200 : OK
    // 内容类型: text/plain
    response.writeHead(200, {'Content-Type': 'text/plain'});

    // 发送响应数据 "Hello World"
    response.end('Hello World\n');
}).listen(8888);

// 终端打印如下信息
console.log('Server running at http://127.0.0.1:8888/');
```
### nodejs([基础教程](http://www.runoob.com/nodejs/nodejs-tutorial.html))
平时工作中使用nodejs机会不多，可以根据提供的教程做一些简单了解就可以了

python \ nodejs 工具推荐[vscode](https://code.visualstudio.com/)
## KO总结一下上面的语言
在各领域都是崭露头角，为不同群体应运而生，都离不开强大的C。
## Linux相关基础应用
### 常用操作命令
#### 查看系统进程
```
ps -ef
```
#### 管道（过滤）
```
ps -ef|grep java
```
#### 查看文件内容
```
1. more
2. view
3. cat
4. tail -1000f file_name
```
#### 目录文件操作
```
1. rm 删除
2. mv 移动
3. cp 拷贝
4. mkdir 创建目录
```
#### 查看命令帮助
```
man 命令 如：man ls
ls --help/-h
```
#### vim操作
[参考链接](http://www.runoob.com/linux/linux-vim.html)

常用的：h j k l /world :wq!

### GIT [基础教程链接](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)
![git图解](img/git.png)
#### 常用命令
1. 添加文件 git add 文件或目录
2. 提交文件 git commit -m ''
3. 上传到远程服务器 git push origin 分支名
4. 拉取最新内容 git pull origin 分支名
5. 初次克隆项目 git clone url
## 课堂链接
. [简单聊聊](session3.md)