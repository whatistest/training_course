# 简单聊聊(framework-autotest)
框架基础TestNG，官网[DOC](http://testng.org/doc/documentation-main.html#introduction)
## 使用关键图
![](img/testng_annotations.png)
## 关键的监听[详细实例文档](https://examples.javacodegeeks.com/enterprise-java/testng/testng-listeners-example/)
1. Introduction to TestNG Listeners
1.1. Example of IExecutionListener
1.2. Example of IAnnotationTransformer
1.2.1. Example of @Test annotation transformer
1.2.2. Example of @DataProvider annotation transformer
1.2.3. Example of @Factory annotation transformer
1.2.4. Example of Configuration annotation transformer
1.3. Example of ISuiteListener
1.4. Example of ITestListener
1.5. Example of IConfigurationListener
1.6. Example of IMethodInterceptor
1.7. Example of IInvokedMethodListener
1.8. Example of IHookable
1.9. Example of IReporter
2. Adding TestNG Listeners
2.1. Adding listeners in testng.xml
2.2. Adding listeners using TestNG @Listeners annotation
2.3. Adding listeners using TestNG API
2.4. Adding listeners using java.util.ServiceLoader
## zbj auto test interface framework
![](img/zbj.png)