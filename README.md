# 重庆米智科技周末之声栏目
哒哒哒的口号：互相学习，互相帮助，实现共赢。
# 栏目套路
- 总结适合自己的套路
- 阶段性实践大于理论
- 编程是门手艺，完爆需要实际
# [**栏目制度**](regulations.md)
# 分享栏目开播时间、地点
1. 时间：隔周日上午九点半点，时长：两小时，以直播形式分享40分钟，再讨论20分钟
2. 直播间：我在钉钉等你哦！[你是不是已经等不及了，那就快快快快快快快点我加入吧!](https://h5.dingtalk.com/invite-page/index.html?code=e904fab6c7)

# 线下沙龙时间、地点（未实施）
1. 时间：每个月最后一周周末
2. 猪八戒会议室
# 分享内容安排
## 基础篇
1. 接口测试（工作中使用的，开源框架[以备后患]）
2. 性能测试（工作中使用的，大量实例）
3. 移动UI自动化测试（无人工干预）
4. 移动兼容性测试（页面、APP）
5. 移动性能测试（android为主）
## 飞升篇
参考霍格沃兹测试学院测试开发课程（https://testerhome.com/topics/12192）
# 常用技能提升学习链接
- [Google机器学习视频](https://developers.google.cn/machine-learning/crash-course/)
- [网易云课堂](http://study.163.com)
- [中国大学慕课](https://www.icourse163.org/)
- [测试攻城狮的家](http://www.testerhome.com)
- [Python学习文档](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)
- [Python学习视频（推荐）](http://bbs.fishc.com)
- [腾讯移动品质中心TMQ](https://testerhome.com/topics/node72)
- [腾讯Bugly](http://my.csdn.net/tencent_bugly)
- [腾讯CSDN博客](http://blog.csdn.net/tmq1225)
![腾讯tmq公众号](session1/img/tmq.jpg)
![百度mtc公众号](session1/img/mtc.png)

没得二维码的好公众号：[Python之美]()，[编程派]()，[Python编程]()，[搜狗测试]()，[阿里技术](), [MQC测试平台]()，[TesterHome]()，[AI前线]()
# 分享栏目主题
课时数 | 主要内容|课堂快车道|分析者
---|---|---|---
2018年07月22日10:00:00 | 1. 关于第四届移动测试开发大会&测试技术管理新发展方向|见钉钉群|李川
2018年07月08日10:00:00 | 1. Mock实际应用 2. 开源接口测试框架（Httprunner）3. 第四届重庆测试沙龙干货|见钉钉群|李川
2018年06月3日10:00:00 | 1. 接口压力测试进阶2 2. Mock应用|[session1](session7/session1.md)  [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|李川
2018年05月20日10:00:00 | 1. 接口压力测试入门 2. 接口压力测试进价1|[session1](session6/session1.md) [session2](session6/session2.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|李川
2018年05月06日10:00:00 | 1. 简述UI自动化流程及思想 2.selenium常用API实操|[session1](session5/session1.md) [session2](session5/session2.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|张航
2018年04月15日10:00:00 | 1. 猪八戒接口框架实战流程 2. Dubbo接口实战|[session1](session4/session.md) [session2](session4/session1.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ)  [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|刘超
2018年04月1日10:00:00 | 1. http接口测试之起步 2. http接口测试之进化|[session1](session3/session1.md) [session2](session3/session2.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|李川
2018年03月18日10:00:00 | 1. 总结上期的问题与不足  2. 相关开发基础知识|[session1](session2/session1.md) [session2](session2/session2.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|李川
2018年03月10日10:00:00 | 1. 接口、性能测试流程  2. 测试环境构建 3. 测试建模|[session1](session1/session1.md) [session2](session1/session2.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx|李川

# 线下沙龙主题（废弃）
课时数 | 主要内容|课堂快车道
---|---|---
2018年05月27日10:00:00 | 交流工作相关问题|[session](session/session1/session.md) [视频链接](https://pan.baidu.com/s/1mfapQloU0jVrxbjw79GqWQ) 密码:3tpx