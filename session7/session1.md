# 接口压力测试进阶2
## 设定场景数据疑问
![](./img/flow.png)
性能测试过程中，比较简单的系统架构图就是上图了。

在数据这一方面，有几个地方是要注意的。
1. DB中的，包含各种类型的数据库；
2. Cache中的，包含各种类型的cache；
3. 压力工具中的，参数化数据。

在执行场景之前，需要知道前两个里面的数据是什么状态。然后再判断压力工具中应该如何加载数据。

其实分析了场景之后也就非常清晰了。就是用真实的数据。那就必须要造出符合业务规则的数据。有几种方式：

1. 在数据库中直接插入数据。可以写SQL，也可以写代码做；我记得在之前的一个项目中，为了往一个表中插入1亿条记录数据，专门写了一段代码。
2. 直接从前台做。也就是做个脚本从前端用压力的方式来预埋数据。这种方式是比较靠谱的，但是要注意的是：1. 产生业务的时间比较集中，所以在做批处理业务的时候要确定下容量是不是合理的；2. 做查询的时候如果涉及到时间范围查询，要关注下是否符合真实业务场景。
3. 从真实环境中导出数据并脱敏。有很多公司都有专门的数据脱敏工具，特别是金融相关系统。
## 参数化
### 参数化目的
为了性能测试过程中，执行的场景更贴近生产环境，我们就需要更逼真的数据做支撑，参数化就是一种较好的方式
### 参数化的几种方式
#### 固定列表
通过一些方法（插库、文件导入）制造数据，将这些数据应用到测试场景中
#### 编码生成
通过编码的方式生成一批数据
#### 变向引导
通过分流的形式将各个环境的数据引导给待测试服务
#### 举例jmeter
1. 用户参数
![](./img/user.png)
2. csv data set config
![](./img/csv.png)
3. 函数
![](./img/function.png)
## 响应时间分析

要分析响应时间，先要说明什么是响应时间，看图
![](./img/time.png)

每次在性能分析之前，都可以画一个这样的图，用以整理自己的思路。

性能的标准究其本质就两个字可以概括：快、慢。

在压力工具中，看到的响应时间，把后面一系列（t1-t18）都包含在内了。所以只拿压力工具中的响应时间来讨论是不可能有结论的，所以拆分响应时间才如此重要。

幸好好像有了如此多的监控工具，才能更好看待这些时间

在大部分情况下，我们都不用关心t1/t2/t4/t6/t8/t11/t13/t15/t17/t18，也就是说除了各业务节点上所消耗的时间外，其他地方出现响应时间的问题的可能性比较小。所以在分析响应时间的时候，我们必须列出查找的优先级，那就是：

优先1[服务自身响应时间]：(t9、t10)、(t7、t12)、(t5、t14)、(t3、t16)

优先级2[网络]：(t8、t11)、(t6、t13)、(t4、t15)、(t2、t17)

优先级3[压力工具]：(t1、t18)

性能压力工具本身产生的响应问题，非常少。但是也并不是不存在，所以我们放到最后来检查。

上面的优先级为什么要加括号呢？看得明白的肯定是一下就能知道就是为了区分进出两部分。

在这样细分了响应时间后，我觉得不会再有找不到响应时间在哪的问题了。

至于如何操作，在上面玄妙的描述中并没有提及，其原因是，每个节点上用的东西不固定，就无法确定如何操作。

但在一个被分析的应用中，上面节点所使用的东西都是确定的，就可以确定操作了，如果谁有确定的系统，但是又不知道的操作的，要知道人世间还有搜索引擎这样亦正亦邪的事务。
