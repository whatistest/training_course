# 接口测试进阶

## 接口 cookie 和 session 机制

1.  为什么需要 session\cookie，深入  了解[参见文章](https://www.zhihu.com/question/19786827)

    HTTP 协议是无状态的协议，服务/客户端需要知道你是谁做出响应的处理，也就发明了 session/cookie，来保存用户信息，传递个服务器识别。

    所有我们在请求接口的时候也要提供响应的认证信息

2.   如何给 http 请求携带认证信息

    总的来就说就是将模拟的认证信息添加到 request heads 中如：
    ![](../session3/img/cookie.png)
    ![](../session3/img/buyer.png)

## 接口响应断言机制

1.  [JsonPath](https://github.com/json-path/JsonPath)
    ![](../session3/img/operators.png)
    ![](../session3/img/filter.png)
    ![](../session3/img/example.png)
    ![](../session3/img/example2.png)

    ```
        <dependency>
            <groupId>com.jayway.jsonpath</groupId>
            <artifactId>json-path</artifactId>
            <version>RELEASE</version>
        </dependency>

        -----------------我是华丽的分割线---------------------

        List<String> authors = JsonPath.read(json, "$.store.book[*].author");
    ```

2.  [assertj](http://joel-costigliola.github.io/assertj/)

```
        <dependency>
        <groupId>org.assertj</groupId>
        <artifactId>assertj-core</artifactId>
        <version>3.9.0</version>
        <scope>test</scope>
        </dependency>

        -----------------华丽的分割线就是我---------------------

        判断是否相等

        判断是否为空
```
## 调用多个API测试
>实际的接口测试过程中，不会全是单独的接口，复杂的业务逻辑，迫使我们需要了解以下几种测试 场景的组建

1. 串串式接口依赖

    串串式接口依赖场景就是：一连串的线性业务操作， 如： 登录到个人数据展示
2. 倒耙式接口依赖

    倒耙式接口依赖场景就是：在线性业务上多出很多分支，如：支付

对于现有zbj接口框架上，做几下几步就可以开启场景接口测试覆盖
1. 在场景中使用的接口都需要添加Scenario注解

```
    @Test(dataProvider = Http.SINGLE, dependsOnMethods = {"com.zhubajie.autotest.cases.example.TestPageServiceHomeNew.testMethod", "com.zhubajie.autotest.cases.example.TestGetServiceSpec.testMethod"})
    @Scenario(methodName = "createTrade2B")
    public void testMethod(Map<String, Object> param, Object result) {
        assertThat(JSONPath.read(result.toString(), "data.taskId")).isNotEqualTo(0);
        assertThat(JSONPath.read(result.toString(), "data.workId")).isNotEqualTo(0);
        assertThat(JSONPath.read(result.toString(), "data.orderId")).isNotEqualTo(0);
    }
```

2. 场景接口之间只存在上下关系的

- 被依赖接口需要添加dependsOnMethods关键字，支持数组

```
@Test(dataProvider = Http.SINGLE, dependsOnMethods = {"com.zhubajie.autotest.cases.example.TestPageServiceHomeNew.testMethod", "com.zhubajie.autotest.cases.example.TestGetServiceSpec.testMethod"})
```

- beforeTest方法中使用context.get(ClassName)获取上一个接口请求返回的值

```
    @Override
    public void beforeTest(Map<String, Object> context) {
        super.beforeTest(context);
        // 获取依赖接口返回的数据
        String APIPageServiceHomeNewResult = context.get("TestpageServiceHomeNew").toString();
        String APIGetServiceSpecResult = context.get("TestgetServiceSpec").toString();

```

3. 场景接口之间存在一对多的情况（多个接口依赖只能被执行一次的接口）

- beforeTest方法中使用SaveResultUtils.getTestDataInCache(ClassName,CASE_INDEX)获取场景中执行过的返回值
