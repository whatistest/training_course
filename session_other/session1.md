# 接口测试入门

## 接口测试基本概念

1. 概述

    接口测试: 模拟终端（browser/app）请求、验证响应数据是否正确的过程.

2. 优点

- 在开发接口尽早暴露出接口的问题，减少前端开发的返工工作量
- 效率(1个接口下的100条用例1秒执行完成)
- 持续(从冒烟到上线监控)

3. 影响

![info](./img/info.png)

## 工具

> 我们不妨先来看下日常工作中的一些常见场景
---
1. 测试或开发人员在定位问题的时候，想调用某个接口查看其是否响应正常；
2. 测试人员在手工测试某个功能点的时候，需要一个订单号， 这个订单号可以通过顺序调用多个接口实现下单流程；
3. 测试人员在开始版本功能测试之前，可以先检测下系统的所有接口是否工作正常，确保接口正常后再开始手工测试；
4. 开发人员在提交代码前需要检测下新代码是否对系统的已有接口产生影响；
5. 项目组需要每天定时检测下测试环境所有接口的工作情况，确保当天的提交代码没有对主干分支的代码造成破坏；
6. 项目组需要定时（30分钟）检测下生产环境所有接口的工作情况，以便及时发现生产环境服务不可用的情况
7. 项目组需要不定期对核心业务场景进行性能测试，期望能减少人力投入，直接复用接口测试中的工作成果。
---

### [Postman](https://www.getpostman.com/docs/v6/) || HTTPClient
1.  get 方式
    - postman
    ![](img/postman.png)
    - iframework
2.  post 方式
    - application/json
    ![](img/json.png)
    - application/x-www-form-urlencoded
    ![](img/url.png)
    - iframework
### [IFramework](http://doc.zhubajie.la/pages/viewpage.action?pageId=22648918)
1.构建基础环境
- 安装jdk
- 安装git
- 安装&配置maven
- 安装&配置用例编辑工具(idea, eclipse)

2.构建脚本环境
- 通过maven骨架配置[方式](http://doc.zhubajie.la/pages/viewpage.action?pageId=23607275)
- 创建项目引用基础项目
## 编写接口用例

>通用步骤
---
1.  找准接口地址及序列化方式

    包括两个地址，一个是根路径（域名），一个是接口路径（/getHotWord）
    application/json,application/x-www-form-urlencoded

2.  完善请求参数

    也就是详细的入参，如：https://m.zbj.com/shunt/favorite?size=50
    参数只有size

3.  得到响应结果

    也就是接口返回参数，如: {"success":true, "content": "", errCode=0, errMsg="返回成功"}

4.  验证规则

    也就是业务逻辑，如上面的接口，size=50，返回的pageSize也要等于50

---
1.使用postman/httpclient单次调试接口

2.zbj流水线接口

- 针对待测接口创建对应测试项目
- 使用通用步骤将业务场景转换为对应脚本
- 与devops流水线拼接
## 测试接口演练

1. 简单入参数据
2. 含有数据库入参数据
3. 单一校验响应数据
4. 需要对比校验数据
5. http && dubbo