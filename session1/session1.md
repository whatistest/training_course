# 第一堂分享：测试流程（接口、性能）
---
## 导读
本节主要从测试人员实践的角度，和大家探讨测试开展方式，包括接口、性能。流程这个东西说大也大，说小也小，关键在于自己去不断借鉴、实践、总结，找到适合自己的测试方法，永远记住：好的不一定合适，合适的一定的最好的。

随着的时间的推移，你会发现思想的比技术略微重要，常说的思想武装头脑大概就就是这个意识！

以下内容仅代表个人理解，重在分享与交流。
## 1. Program flow
![test flow](img/input&output.png)
- 说到流程，作为测试来说就会想到经典定义：在规定的条件下对程序进行操作，以发现程序错误，衡量软件质量，并对其是否能满足设计要求进行评估的**过程**
- 用我的话说：测试就是验证程序是不是符合实际需求，程序员是建造者，测试是破坏者，现在进入到测试左移。
- 讨论你对测试的理解？
## 2. 接口测试流程
### 接口测试的好处
- 在开发接口尽早暴露出接口的问题，减少前端开发的返工工作量
- 效率(1个接口下的100条用例1秒执行完成)
- 持续(从冒烟到上线监控)
### 简一流程图
![interface](img/interface.png)
- 分析需求：了解业务，了解程序设计
- 规划场景：使用不同测试方法设计场景，提高测试覆盖、效率，如：等价划分，边界值
- Mock分离：mock依赖接口之间的数据
- 构建场景：调用不同方式去实践场景，如：coding, web
- 持续集成：通过一系列流水线工具定时集成构建的场景，如：[jenkins](https://baike.baidu.com/item/Jenkins/10917210?fr=aladdin),[travis-ci](travis-ci.org)

> 注意事项

接口测试的本质是通过测试参数的排列组合验证返回值/数据库变更是否符合预期，从而确定接口相关代码是否正确。从业务角度考虑的意思是这些排列组合不是随便想出来，而是业务中有可能出现的排列组合。

和功能测试不同的是：需要你通过各种方式去请求接口、拿数据、验证数据
方式：手动填接口参数、录制请求接口回放

### 用例编写、开始执行  [优秀接口用例设计参考链接](https://testerhome.com/topics/11677)
- 开始编写前置条件、入参、返回结果、预期结果等测试用例相关信息。
- 运用工具去执行测试用例。
- 校验返回结果是否正确。 -简单的一个 接口测试完成。

> 举个栗子(微信发朋友圈接口)

1. 功能用例
```
用上传图片接口上传图片，验证图片是否上传成功，各 url 对应文件的 md5 是否和我本地上传的图片的 md5 吻合。
用发本地圈接口发本地圈，其中图片 url 来自第一步的返回值
用查看本地圈接口查看发出的本地圈，检查它的内容、图片是否正确。
```

2. 接口用例
```
有图片、有文字，预期返回发布成功
无图片，有文字，预期返回发布成功
有图片，无文字，预期返回发布成功
无图片，无文字，预期返回发布失败
有图片、有文字，预期返回发布成功
```

### 接口分析、工具选择
- 目前主流的协议HTTP、REST、Double，还有一些个别公司自己开发的协议。
- 其实我们做接口测试对于是什么协议前期不用太怎么关注，我们主要关注怎么模拟发送请求。
当我们确定接口是哪些协议的时候，那么我们就开始选择模拟调用端了。
- 基于HTTP接口测试的工具:restclient、[postman](https://testerhome.com/topics/6695)、jmeter、soapUI等等，基于JAVA代码的httpclinet、JAVA自带的URLConnection等。
- Dubbo之类的可能要自己客户端请求代码了。
- 所以接口测试第一件事情，确定客户端或者调用工具。



### 常用套路
1. 总结常用的接口入参校验，比如：不能为空、大于某个值
2. 测试过程中发现问题，先从异常找原因
3. 总结自己的套路帮助快速捡起来

### 举几个栗子
1. 请求http://cnodejs.org/api get /topics 主题首页
2. 请求https://m.zbj.com/ /task/getDate4perfectTask post接口
3. 请求dobbo 接口 https://gitee.com/whatistest/Alien.git
## 3. 性能测试流程(涉及到server/app)
![performace](img/performace.png)
- 性能指标：确认本次程序，给定了哪些指标，或者需求中剥离、提炼（如：一个线上消息推送系统，需要你通过测试，数据量最高峰在1000万/小时的时候，要启动多少台机器）；有时候会发现业务方没有实质性的性能，就需要你的经验了来提供。
常用服务器指标->内存、CPU、网络、电量、流量、速度/耗时
- 性能场景：用户使用场景多种多样，全部覆盖不太可能，分析用户行为选择合适的场景
- 性能工具：
    - 模拟数据：[jmeter](http://jmeter.apache.org/) [locust](https://www.locust.io/) [ngrinder](http://naver.github.io/ngrinder/) [全链路压测](http://www.infoq.com/cn/articles/jd-618-upgrade-full-link-voltage-test-program-forcebot)
    - 搜集数据：[heapanalyzer](https://www.ibm.com/developerworks/community/groups/service/html/communityview?communityUuid=4544bafe-c7a2-455f-9d43-eb866ea60091) [jprofiler](https://www.ej-technologies.com/products/jprofiler/overview.html) [gt](http://gt.qq.com/) [adb](https://developer.android.com/studio/command-line/adb.html?hl=zh-cn) [pingpoint](http://note.youdao.com/noteshare?id=925f02978e4dbab5583b38bcd6eb32fb) [zabbix](https://www.zabbix.com/)

### 举个栗子：
SMS调优
## 总结
1. 知己知彼，百战百胜
2. 流程很重要
## 练习
1. 总结属于你的测试流程
## 相关链接
## 课堂链接
. [第二节课](session2.md)