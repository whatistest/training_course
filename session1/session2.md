# 第二课：测试环境构建及测试建模
---
## 1. 测试接口环境
### JAVA类型接口框架
- 第一步：安装jdk，[win图文链接](https://jingyan.baidu.com/article/3c343ff70bc6ea0d377963df.html) [linux图文链接](https://jingyan.baidu.com/article/48a42057f1f0a4a925250464.html)
- 第二步：安装配置maven [win图文链接](http://www.cnblogs.com/liuhongfeng/p/5057827.html)
- 第二步：安装工具 [eclipse链接](http://www.eclipse.org/) [idea链接](https://www.jetbrains.com/idea/) [idea码链接](idea.lanyus.com)
### python类型接口框架
- 第一步：安装python [下载链接](https://www.python.org/downloads/)
- 第二步：安装pip [下载链接](https://pip.pypa.io/en/stable/installing/#python-and-os-compatibility)
- 第三步：安装pyenv管理多个版本 [下载链接](https://github.com/pyenv/pyenv) [使用说明](https://zhuanlan.zhihu.com/p/27294128)
- 第四步：安装工具 [vscode](https://code.visualstudio.com/)
### 手动调试接口工具
- postman [下载链接](https://www.getpostman.com/)
- httpclient(firefox应用商店下载)
## 2. 测试性能环境
## jmeter
- 安装jdk
- 下载jmeter [下载链接](http://jmeter.apache.org/)
- 如果是扩展的协议，将扩展的jar包放在ext/lib
## 3. 测试建模
我了解测试建模是从这篇[文章](http://blog.csdn.net/tmq1225/article/details/53521272)开始的，希望分享给大家
## 本节总结
1. 一些基础测试环境的搭建说明
2. 多实践多交交流
## 练习
1. 本机搭建好测试接口、性能环境
2. 熟练用工具调用接口
3. 学完java基础教程第一部分
## 相关链接
1. java基础教程 [文本链接](http://www.runoob.com/java/java-tutorial.html)[视频链接](http://study.163.com/category/java?utm_source=baidu&utm_medium=cpc&utm_campaign=affiliate&utm_term=IT_152a&utm_content=SEM)
2. python学习[文本链接](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000) [视频链接](https://pan.baidu.com/s/1htBleUK )视频密码: in9c
3. 老司机学jmeter：链接：http://pan.baidu.com/s/1hsoTQkO 密码：pzkh
