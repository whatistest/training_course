# Easy Mock

## 概述

用可视化的方式快速生成模拟数据并持久化

## 项目信息

1. 访问地址： www.easy-mock.com

2. 任意用户名都可登录

3. 详细文档：easy-mock.com/docs

## 适用范围

1. 测试 easy-mock ，模拟场景数据

2. 前端开发 easy-mock cli ，模拟接口调试数据

## 使用场景

1. 一个接口返回一种数据

2. 一个接口返回多种数据（根据不同条件）

## 模拟数据仅需要以下几步

- 创建被模拟接口响应数据

![](./img/create-api.png)
![](./img/response.png)

- 配置代理工具

![](./img/charles.png)
![](./img/mock-rule.png)

- 配置浏览器代理

![](./img/proxy.png)

## Mock & proxy原理

![](./img/anyproxy.png)